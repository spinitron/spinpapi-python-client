SpinPapi-python
=====

A client class for Spinitron's SpinPapi API in python

SpinPapi documentation: [http://spinitron.com/user-guide/pdf/SpinPapi-v2.pdf](http://spinitron.com/user-guide/pdf/SpinPapi-v2.pdf)

## Usage
    from SpinPapiClient import SpinPapiClient
    client = SpinPapiClient('your_auth_user_id', 'your_auth_secret')
    sys.stdout.write(client.query({'method': 'getSong', 'station': 'wzbc'}))



